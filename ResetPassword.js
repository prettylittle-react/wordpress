import React from 'react';
import PropTypes from 'prop-types';

import Form from 'form';

import * as yup from 'yup';

/**
 * Register
 * @description [Description]
 * @example
  <div id="Register"></div>
  <script>
    ReactDOM.render(React.createElement(Components.Register, {
        title : 'Example Register'
    }), document.getElementById("Register"));
  </script>
 */
class ResetPassword extends Form {
	getInitialValues() {
		return {
			password: '',
			password_repeat: ''
		};
	}

	// https://itnext.io/simple-react-form-validation-with-formik-yup-and-or-spected-206ebe9e7dcc
	getFormValidationSchema() {
		return yup.object({
			password: yup
				.string()
				.required('Password is required')
				.min(8, 'Password is too short - should be 8 chars minimum.'),
			password_repeat: yup.string().oneOf([yup.ref('password')], 'Passwords do not match')
		});
	}

	renderFields() {
		return (
			<div className={`${this.baseClass}`} ref={component => (this.component = component)}>
				<h3>Reset Password</h3>
				<Form.Password label="Password" name="password" />
				<Form.Password label="Repeat Password" name="password_repeat" />
			</div>
		);
	}
}

ResetPassword.defaultProps = {
	...Form.defaultProps,
	...{
		submitLabel: 'Reset Password'
	}
};

ResetPassword.propTypes = {
	...Form.propTypes,
	...{}
};

export default ResetPassword;
