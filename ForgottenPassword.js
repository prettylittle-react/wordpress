import React from 'react';
import PropTypes from 'prop-types';

import Form from 'form';

import * as yup from 'yup';

/**
 * Register
 * @description [Description]
 * @example
  <div id="Register"></div>
  <script>
    ReactDOM.render(React.createElement(Components.Register, {
        title : 'Example Register'
    }), document.getElementById("Register"));
  </script>
 */
class ForgottenPassword extends Form {
	getInitialValues() {
		return {
			username: ''
		};
	}

	// https://itnext.io/simple-react-form-validation-with-formik-yup-and-or-spected-206ebe9e7dcc
	getFormValidationSchema() {
		return yup.object({
			username: yup.string().required('Bruh, username is required')
		});
	}

	renderFields() {
		return (
			<div className={`${this.baseClass}`} ref={component => (this.component = component)}>
				<h3>Forgotten Password</h3>
				<Form.Text label="Username or Email" name="username" />
			</div>
		);
	}
}

ForgottenPassword.defaultProps = {
	...Form.defaultProps,
	...{
		submitLabel: 'Reset Password'
	}
};

ForgottenPassword.propTypes = {
	...Form.propTypes,
	...{}
};

export default ForgottenPassword;
