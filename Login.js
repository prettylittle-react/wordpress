import React from 'react';
import PropTypes from 'prop-types';

import {Content, Editable, Heading, Img, RichText, Text} from 'editable';

import Form from 'form';

import * as yup from 'yup';

// https://developer.wordpress.org/rest-api/using-the-rest-api/authentication/
// https://www.lee-harris.co.uk/blog/wordpress-ajax-login-without-using-a-plugin/

/**
 * Login
 * @description [Description]
 * @example
  <div id="Login"></div>
  <script>
    ReactDOM.render(React.createElement(Components.Login, {
        title : 'Example Login'
    }), document.getElementById("Login"));
  </script>
 */
class Login extends Form {
	getInitialValues() {
		return {
			username: '',
			password: ''
		};
	}

	getFormValidationSchema() {
		return yup.object({
			username: yup.string().required('Bruh, username is required'),
			password: yup.string().required('Password is required')
		});
	}

	renderFields() {
		return (
			<div className={`${this.baseClass}`} ref={component => (this.component = component)}>
				<h3>Login</h3>
				<Form.Text label="Username or Email" name="username" />
				<Form.Password label="Password" name="password" />
				<RichText>
					<p>
						or <a href="#register">Register</a>
					</p>
				</RichText>
			</div>
		);
	}
}

Login.defaultProps = {
	...Form.defaultProps,
	...{
		submitLabel: 'Login'
	}
};

Login.propTypes = {
	...Form.propTypes,
	...{}
};

export default Login;
