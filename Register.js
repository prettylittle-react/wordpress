import React from 'react';
import PropTypes from 'prop-types';

import {Content, Editable, Heading, Img, RichText, Text} from 'editable';

import Form from 'form';

import * as yup from 'yup';

import {sleep} from 'libs/utils';

/**
 * Register
 * @description [Description]
 * @example
  <div id="Register"></div>
  <script>
    ReactDOM.render(React.createElement(Components.Register, {
        title : 'Example Register'
    }), document.getElementById("Register"));
  </script>
 */
class Register extends Form {
	getInitialValues() {
		return {
			username: '',
			password: ''
		};
	}

	// https://itnext.io/simple-react-form-validation-with-formik-yup-and-or-spected-206ebe9e7dcc
	getFormValidationSchema() {
		return yup.object({
			username: yup.string().required('Bruh, username is required'),
			password: yup
				.string()
				.required('Password is required')
				.min(8, 'Password is too short - should be 8 chars minimum.')
		});
	}

	validateUsername = value => {
		if (value === 'admin') {
			return 'Nice try!';
		}

		/*

		return sleep(500)
			.then(result => {
				// return;//'username or email address already exists';
			})
			.catch(error => {})
			.finally(() => {});
			*/
	};

	renderFields() {
		return (
			<div className={`${this.baseClass}`} ref={component => (this.component = component)}>
				<h3>Register</h3>
				<Form.Text label="Firstname" name="firstname" />
				<Form.Text label="Surname" name="surname" />
				<Form.Text label="Username or Email" name="username" validate={this.validateUsername} />
				<Form.Password label="Password" name="password" />
				<RichText>
					<p>
						or <a href="#login">Login</a> if you are already registered
					</p>
				</RichText>
			</div>
		);
	}
}

Register.defaultProps = {
	...Form.defaultProps,
	...{
		submitLabel: 'Register'
	}
};

Register.propTypes = {
	...Form.propTypes,
	...{}
};

export default Register;
